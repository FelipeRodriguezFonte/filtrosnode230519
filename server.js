require('dotenv').config();
const express = require('express');

const app = express();

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}


const io = require('./io');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

//Intentará hacer un parseo automatico a json
app.use(express.json());

app.use(enableCORS);

//Si en entorno tengo un puerto lo uso, sino uso el 300
const port = process.env.PORT || 3000;

app.listen(port);
console.log("API escuchando en el puerto.... " + port);

//get -> verbo HTTP
app.get("/apitechu/v1/hello",
    function(req, res) {
      console.log("GET /apitechu/v1/hello");

//Así se enviaría como json
      res.send({"msg": "Hola desde API TechU"});
    }

)


//get -> verbo HTTP
app.get("/apitechu/v1/users", userController.getUsersV1);
//get -> verbo HTTP
app.get("/apitechu/v2/users", userController.getUsersV2);
//get -> verbo HTTP
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);

//post -> verbo HTTP
app.post("/apitechu/v1/users",userController.createUsersV1);
//post -> verbo HTTP
app.post("/apitechu/v2/users",userController.createUsersV2);

//delete -> verbo HTTP
app.delete("/apitechu/v1/users/:id",userController.deleteUsersV1);
app.delete("/apitechu/v2/users/:id",userController.deleteUsersV2);

app.post("/apitechu/v1/login",authController.loginV1);
app.post("/apitechu/v1/logout/:id",authController.logoutV1);

app.post("/apitechu/v2/login",authController.loginV2);
app.post("/apitechu/v2/logout/:id",authController.logoutV2);

//get -> verbo HTTP
app.get("/apitechu/v2/accounts/:id", accountController.getAccountByIdV2);


function search(arrayUsers, id){

  //Opción 1 - bucle for de toda la vida.
  // var i;
  //
  // for (i=0;i<arrayUsers.length;i++){
  //
  //   if (arrayUsers[i].id==id){
  //     console.log(arrayUsers[i]);
  //     console.log(arrayUsers[i].id);
  //     arrayUsers.splice(i,1);
  //   }
  // }

  //Opción 2 - for in
  // for (var i in arrayUsers){
  //      if (arrayUsers[i].id==id){
  //       console.log(arrayUsers[i]);
  //       console.log(arrayUsers[i].id);
  //       arrayUsers.splice(i,1)
  //     }
  // }

  //Opción 3 - for search
  // var i = 0;
  // var swFound = false;
  // arrayUsers.forEach (function(user) {
  //
  //       if (user.id==id){
  //         console.log("found... " + i );
  //         console.log(id);
  //         swFound = true;
  //       }else{
  //         if (swFound == false){
  //           i = i+1;
  //         }
  //
  //       }
  //
  // })
  // console.log("Tras salir ..." + i);
  // if (swFound){
  //     arrayUsers.splice(i,1);
  // }

//Opción 4
  // var i = 0;
  // var swFound = false;
  //
  // for (user of arrayUsers){
  //    if (user.id==id){
  //       swFound = true;
  //       break;
  //    }
  //    i = i+1;
  // }
  // if (swFound){
  //   arrayUsers.splice(i,1);
  // }

//Opcion 5

  var i = arrayUsers.findIndex(x => x.id == id)
  arrayUsers.splice(i,1);

  return arrayUsers;

};



app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("Parametros");
    console.log(req.params);

    console.log("Query string");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);


  }

);
