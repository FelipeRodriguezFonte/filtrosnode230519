const io = require('../io');
const crypt = require("../crypt");
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufrf13ed/collections/";
const mLabAPIKey  = "apiKey=" + process.env.MLAB_API_KEY;

//-- Obtenemos el usuario en base a su id
function getAccountByIdV2(req, res){
  console.log("GET /apitechu/v2/accounts/:id");
  // var id = req.params.id;
  // console.log("La id del usuario a buscar es " + id);
  // var query = 'q={"id":' + id + '}';

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a buscar es " + id);
  var query = "q=" + JSON.stringify({"id":id});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");
  httpClient.get("accounts?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
        if (err){
          var response = { "msg" : "Error obteniendo cuenta de usuario"}
          res.status(500);
        }else{
          if (body.length > 0){
            var response = body;
          }else{
            var response = {"msg" : "Usuario sin cuenta"}
            res.status(404);
          }

        }
        res.send(response);
      }
    )


}

module.exports.getAccountByIdV2 = getAccountByIdV2;
