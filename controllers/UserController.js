const io = require('../io');
const crypt = require("../crypt");
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufrf13ed/collections/";
const mLabAPIKey  = "apiKey=" + process.env.MLAB_API_KEY;


function getUsersV1(req, res) {
  console.log("GET /apitechu/v1/users");
  console.log("Parametro count: " + req.query.$count);
  var usersCount = req.query.$count;

  console.log("Parametro top: " + req.query.$top);
  var usersTop = req.query.$top;

  //console.log("Parametros en general: " + req.query);

  var users = require('../usuarios.json');
  var resp = {};

  if (usersCount == 'true')
  {
    console.log("Es valor true... y el valor es: " + users.length );
    resp = {"count": users.length };
    console.log(resp);

  }

  if ((usersTop != null ) && (usersTop > 0)){
    console.log("No Es nulo o cero");
    resp.users = users.slice(0,usersTop);
    console.log(resp);
  } else{
    resp.users = users;
  }

  //var resp2 = "hola";
//  res.send(resp2);
  res.send(resp);

}

//TODO fix no esta devolviendo todos los usuarios....
function getUsersV2(req, res) {
    console.log("GET /apitechu/v2/users");

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");

    httpClient.get("user?" + mLabAPIKey,
      function(err,resMLab,body){

          if (err){
            var response = { "msg" : "Error obteniendo usuario"}
            res.status(500);
          }else{
            if (body.length > 0){
              var response = body;
            }else{
              var response = {"msg" : "Usuario no encontrado"}
            }
            res.status(404);
          }

        res.send(response);
      }

    )
}

//-- Obtenemos el usuario en base a su id
function getUsersByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");
  // var id = req.params.id;
  // console.log("La id del usuario a buscar es " + id);
  // var query = 'q={"id":' + id + '}';

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a buscar es " + id);
  var query = "q=" + JSON.stringify({"id":id});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
        if (err){
          var response = { "msg" : "Error obteniendo usuario"}
          res.status(500);
        }else{
          if (body.length > 0){
            var response = body[0];
          }else{
            var response = {"msg" : "Usuario no encontrado"}
            res.status(404);
          }

        }
        res.send(response);
      }
    )


}

///--- Función de crear usuarios
function createUsersV1(req, res) {
  console.log("POST /apitechu/v1/users");
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  console.log(newUser);

  var users = require('../usuarios.json');
// Añadimos un usuario al final del array
  users.push(newUser);
  console.log("Usuario añadido al array");

  io.writeUserDataToFile(users);
  console.log("Proceso de creación de usuario finalizado");


  }


  function createUsersV2(req, res){
    console.log("POST /apitechu/v2/users");
    console.log(req.body.id);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);
    //quitar password
    console.log(req.body.password);
    var newUser = {
      "id": req.body.id,
      "first_name":req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
    }

    var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Client created");
    httpClient.post("user?" + mLabAPIKey,newUser,
      function(err, resMLab,body){
        console.log("Usuario creado en Mlab");
        res.status(201).send({"msg":"Usuario creado"});
      }
  )


  }






  function deleteUsersV1(req, res){
    console.log("DELETE /apitechu/v1/users/:id");
    console.log("id del usuario a borrar es " + req.params.id);

    var users = require("./usuarios.json");
    var usersToWrite = search(users,req.params.id);
    console.log(usersToWrite);
    // console.log("Antes de borrar");
    // console.log(users);
    // users.splice(req.params.id - 1, 1);
    // console.log(users);
    // console.log("Despues de borrar");
    // console.log("Usuario quitado del array");
    io.writeUserDataToFile(users);

  }



  function deleteUsersV2(req, res){
      console.log("DELETE /apitechu/v2/users");
      console.log("id del usuario a borrar es " + req.params.id);

      var httpClient = requestJson.createClient(baseMlabURL);
      console.log("Client created");
      var q = "q={'id':" + req.params.id + "}";
      console.log(q);

    //
      httpClient.put("user?" + q +'&' + mLabAPIKey,[{}],
        function(err, resMLab,body){
          if (err){
            console.log("Error" + err);
          }else{
          console.log("Usuario eliminado en Mlab");
          console.log(resMLab);
          res.status(201).send({"msg":"Usuario eliminado"});
        }
        }
    )


}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUsersV1 = createUsersV1;
module.exports.createUsersV2 = createUsersV2;
module.exports.deleteUsersV1 = deleteUsersV1;
module.exports.deleteUsersV2 = deleteUsersV2;
