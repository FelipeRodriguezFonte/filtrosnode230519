const io = require('../io');
const crypt = require("../crypt");
const requestJson = require('request-json');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechufrf13ed/collections/";
const mLabAPIKey  = "apiKey=" + process.env.MLAB_API_KEY;




function loginV1(req, res){
  console.log("POST /apitechu/v1/login");
  console.log("El usuario para loguear es: " + req.body.email);
  console.log("La password es: " + req.body.password)

  //Validamos si existe el usuario y si la pass es correcta
  var users = require("../usuarios.json");
  var count = -1;
  count = searchUsers(users,req.body.email,req.body.password);
  var resp = {};

  if (count > -1){
        console.log("Se ha encontrado");
        console.log(users[count]);
        users[count].logged = true;
        io.writeUserDataToFile(users);
        console.log(users[count]);
//        resp = {"msg": "Login OK"};
        resp.msg = "Login OK";
        resp.id = users[count].id;
  } else{
        console.log("No se ha encontrado");
        resp = {"msg": "Login KO"};
  }
  res.send(resp);
}

function searchUsers(users,email,password){
    var counter = -1;
    console.log("El email es " + email);
    for (i=0;i<users.length;i++){
  //   Lo hago así por eficiencia, busco el email y si existe evaluo y salgo
      if (users[i].email == email){
        console.log("Tengo el mismo email");
        console.log(users[i].email);
        //Verifico la password
        if (users[i].password == password ){
           counter = i;
        }
        break;
      }
    }
    return counter;
}
//Generamos Login v2, gestionando los datos con MongoDB

function loginV2(req, res){
  console.log("POST /apitechu/v2/login");
  console.log("El usuario para loguear es: " + req.body.email);
  console.log("La password es: " + req.body.password);

  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var query = "q=" + JSON.stringify({"email":req.body.email});

  httpClient.get("user?" + query + "&" +mLabAPIKey,
    function(err,resMLab,body){

        if (err){
          console.log("Error obteniendo usuario");
          res.status(500).send();
        }else{
          if (body.length > 0){
            var response = body[0];
            //Aqui tengo el usuario.
            console.log(body[0]);
            console.log(body[0].email);
            console.log(body[0].password);

          //  var verif = verificar(usuario,password);
            var verific = crypt.checkPassword(req.body.password, body[0].password);
            if (verific) {
              console.log("Se ha verificado bien");
              var bodySetteo = {"$set":{"logged":true}};

              httpClient.put("user?" + query +'&' + mLabAPIKey,bodySetteo,
                function(err, resMLabUp,bodyUp){
                  if (err){
                    console.log("Error" + err);
                    res.status(500).send();
                  }else{
                  console.log("Usuario actualizado en Mlab");
                  var respuestaUpd = {};
                  respuestaUpd.msg = "Login OK";
                  respuestaUpd.id = body[0].id;

                  res.send(respuestaUpd);
                }
                }
            )

            }else{
              console.log("No se ha verificado");
              res.status(404).send({"msg" : "KO Login"});
            }


          }else{
            console.log("Usuario no encontrado");
            res.status(404).send({"msg" : "KO Login"});
          }

        }


    }

  )
}


function logoutV1(req, res){
  console.log("POST /apitechu/v1/logout/:id");
  console.log("id del usuario a borrar es " + req.params.id);

  var users = require("../usuarios.json");
  var count = -1;
  count = searchUsersById(users,req.params.id);

  var resp = {};

  if (count > -1){
        console.log("Se ha encontrado, procedemos a desloguear");
        console.log(users[count]);

//control por si la variable no existe no intentarla borrar
        if (users[count].logged){
            delete users[count].logged;
        }

        io.writeUserDataToFile(users);

        resp.msg = "Logout OK";
        resp.id = users[count].id;
  } else{
        console.log("No se ha encontrado");
        resp = {"msg": "Logout KO"};
  }
  res.send(resp);

}

function logoutV2(req, res){
  console.log("POST /apitechu/v2/logout");
  console.log("El usuario para desloguear es: " + req.params.id);


  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Client created");

  var query = "q=" + JSON.stringify({"id":Number.parseInt(req.params.id)});

  httpClient.get("user?" + query + "&" +mLabAPIKey,
    function(err,resMLab,body){

        if (err){
          console.log("Error obteniendo usuario");
          res.status(500).send();
        }else{
          if (body.length > 0){
            var response = body[0];
            //Aqui tengo el usuario.
            console.log(body[0]);
            console.log(body[0].email);

              console.log("Se ha verificado bien");
              var bodySetteo = {"$unset":{"logged":""}};
              console.log(bodySetteo);

              httpClient.put("user?" + query +'&' + mLabAPIKey,bodySetteo,
                function(err, resMLabUp,bodyUp){
                  if (err){
                    console.log("Error" + err);
                    res.status(500).send();
                  }else{
                  console.log("Usuario actualizado en Mlab");
                  var respuestaUpd = {};
                  respuestaUpd.msg = "Logout OK";
                  respuestaUpd.id = body[0].id;

                  res.status(201).send(respuestaUpd);
                }
                }
            )
          }else{
            console.log("Usuario no encontrado");
            res.status(404).send({"msg" : "KO Login"});
          }

        }


    }

  )
}




function searchUsersById(users,id){
    var counter = -1;
    for (i=0;i<users.length;i++){
  //   Lo hago así por eficiencia, busco el email y si existe evaluo y salgo
      if (users[i].id == id){
        //Verifico el id
           counter = i;
        break;
      }

    }
    return counter;
}



module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
